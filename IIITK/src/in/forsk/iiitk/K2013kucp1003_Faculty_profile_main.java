package in.forsk.iiitk;

import in.forsk.iiitk.adapter.K2013kucp1003_Faculty_list_adapter;
import in.forsk.iiitk.wrapper.K2013kucp1003_Faculty_Wrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

public class K2013kucp1003_Faculty_profile_main extends Activity {

	public final static String TAG = FacultyProfile.class.getSimpleName();
	private Context context;
    String response="";
    String url;
	
	ImageView mMenuIcon;

	ListView mFacultyList;
	K2013kucp1003_Faculty_list_adapter mAdapter;

	ArrayList<K2013kucp1003_Faculty_Wrapper> mFacultyDataList = new ArrayList<K2013kucp1003_Faculty_Wrapper>();

	Button mBackBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k2013kucp1003__faculty_profile_main);

		context = this;
		mFacultyList = (ListView) findViewById(R.id.facultyList);
		mBackBtn = (Button) findViewById(R.id.backBtn);

		parseRemoteFacultyFile(" ");

		mBackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	public void printObject(K2013kucp1003_Faculty_Wrapper obj) {
		// Operator Overloading
		Log.d(TAG, "Name : " + obj.getname());
		Log.d(TAG, "Photo : " + obj.getPhoto());
		Log.d(TAG, "Department : " + obj.getDepartment());
		Log.d(TAG, "reserch_area : " + obj.getReserch_area());
		Log.d(TAG, "Phone : " + obj.getPhone());
		Log.d(TAG, "Email : " + obj.getEmail());
		
		
	}

	private String openHttpConnection(String urlStr) throws IOException {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL("http://online.mnit.ac.in/iiitk/assets/faculty.json");
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertStreamToString(in);
	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

	

	public void setK2013kucp1003_Faculty_list_adapter(ArrayList<K2013kucp1003_Faculty_Wrapper> mFacultyDataList) {
		mAdapter = new K2013kucp1003_Faculty_list_adapter(context, mFacultyDataList);
		mFacultyList.setAdapter(mAdapter);
	}

	public ArrayList<K2013kucp1003_Faculty_Wrapper> pasreLocalFacultyFile(String json_string) {

		ArrayList<K2013kucp1003_Faculty_Wrapper> mFacultyDataList = new ArrayList<K2013kucp1003_Faculty_Wrapper>();
		try {
			// Converting multiple json data (String) into json array
			JSONArray facultyArray = new JSONArray(json_string);
			Log.d(TAG, facultyArray.toString());
			// Iterating json array into json objects
			for (int i = 0; facultyArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

				// Design patterns
				K2013kucp1003_Faculty_Wrapper facultyObject = new K2013kucp1003_Faculty_Wrapper(
						facultyJsonObject);

				//printObject(facultyObject);

				mFacultyDataList.add(facultyObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mFacultyDataList;
	}

	public void parseRemoteFacultyFile(final String url) {
		new FacultyParseAsyncTask().execute(url);
	}

	// Params, Progress, Result
	class FacultyParseAsyncTask extends AsyncTask<String, Integer, String> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(context);
			pd.setMessage("loading..");
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {

				response = openHttpConnection(url);

				onProgressUpdate(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			ArrayList<K2013kucp1003_Faculty_Wrapper> mFacultyDataList = pasreLocalFacultyFile(result);
			setK2013kucp1003_Faculty_list_adapter(mFacultyDataList);

			if (pd != null)
				pd.dismiss();
		}
	}

	

}


