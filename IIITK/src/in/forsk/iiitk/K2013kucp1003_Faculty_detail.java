package in.forsk.iiitk;

import com.androidquery.AQuery;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class K2013kucp1003_Faculty_detail extends Activity {
	
    static String header="";
    static String photo="";
    static String name="";
    static String department="";
    static String hometown="";
    static String email="";
    static String phone="";
    static String course="";
    static String designation="";
    static String fb="";
    static String qualifications="";
    static String ach="";
    
    TextView view_header,view_name,view_department,view_hometown,view_email,view_phone,view_courses,view_des,view_fb,view_qual,view_ach;
    Bitmap bm;
    ImageView view_photo;
    AQuery aq;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.k2013kucp1003_faculty_detail);

        aq = new AQuery(context);

        view_name = (TextView)findViewById(R.id.textView4);
        view_header = (TextView)findViewById(R.id.textView1);
        view_department = (TextView)findViewById(R.id.textView7);
        view_hometown = (TextView)findViewById(R.id.textView17);
        view_email = (TextView)findViewById(R.id.textView19);
        view_phone = (TextView)findViewById(R.id.textView18);
        view_photo = (ImageView)findViewById(R.id.imageView2);
        view_courses = (TextView)findViewById(R.id.textView10);
        view_des = (TextView)findViewById(R.id.textView6);
        view_fb = (TextView)findViewById(R.id.textView12);
        view_qual = (TextView)findViewById(R.id.textView23);
        view_ach = (TextView)findViewById(R.id.textView24);
        
        //view_photo.setImageBitmap(bm);
        view_header.setText(name);
        view_name.setText(name);
        view_department.setText(department);
        view_hometown.setText(hometown);
        view_email.setText(email);
        view_phone.setText(phone);
        view_courses.setText(course);
        view_fb.setText(fb);
        view_qual.setText(designation);
        view_ach.setText(ach);
        view_des.setText(designation);

        AQuery aq = new AQuery(getApplicationContext());
        aq.id(R.id.imageView2).image(photo,true,true, 0, R.drawable.ic_launcher );

    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getLayoutInflater().inflate(R.layout.k2013kucp1003_faculty_detail, layout);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //non inspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/}
